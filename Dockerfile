FROM python:3.11.9-alpine3.19

COPY requirements.txt /

# Tried apk add py3-requests py3-paho-mqtt, but python imports failed :/
RUN pip3 install -r /requirements.txt \
    && mkdir /pollen \
    && rm /requirements.txt

COPY pollenpoller.py /pollen

WORKDIR /pollen

ENV REGION Svergie
ENV MQTT_HOST localhost
ENV MQTT_PORT 1883
ENV MQTT_USER kurt
ENV MQTT_PASS 2mad
ENV HA_BASE homeassistant
ENV DEV_NAME pollenpoller

CMD python3 pollenpoller.py
