# PollenPoller

Accesses the API from Pollen Rapporten and pushes to Home-Assistant
over MQTT.

If data gets published see link below for attribution:
https://pollenrapporten.se/omwebbplatsen/attanvandapollenprognoserna.4.314e02dd13d69872ec0201.html

API Documentation:
https://api.pollenrapporten.se/docs

## Requirements

If not builiding and using the OCI / docker image then see
[requirements.txt](requirements.txt).

## Configuration

Is done with env variables, please see the [Dockerfile](Dockerfile)
for expanded defaults.

| ENV         | Default         |
|-------------|-----------------|
| `REGION`    |                 |
| `MQTT_HOST` |                 |
| `MQTT_PORT` | `1883`          |
| `MQTT_USER` |                 |
| `MQTT_PASS` |                 |
| `HA_BASE`   | `homeassistant` |
| `DEV_NAME`  | `pollenpoller`  |

If `REGION` is missconfigured an error will list currently available regions.

`HA_BASE` is the base path of the MQTT topic for home-assistant, since
this can be changed. But it's probably best to leave as default.

`DEV_NAME` is what the script will base IDs etc on and for
Home-Assistant. It doesn't change the name in Home-assistant at the
moment.
