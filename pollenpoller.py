#!/usr/bin/env python3
# PollenPoller pulls pollen forcasts from Pollenrapport.se API to MQTT.
# Copyright (C) 2024  Avraham Lembke <blambi@ulthar.se>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import requests
import json
import datetime
import os
import paho.mqtt.publish as mqtt_pub
import time
import sys
import logging


class Forecast:
    region = None
    region_id = None
    start_date = None
    end_date = None
    text = None
    pollen = None  # Series

    def __init__(self, pollen_inst, raw_forecast, region):
        self.region = region
        self.region_id = raw_forecast.get("regionId")
        self.start_date = datetime.datetime.fromisoformat(raw_forecast.get("startDate"))
        self.end_date = datetime.datetime.fromisoformat(raw_forecast.get("endDate"))
        self.text = raw_forecast.get("text")
        self.pollen = list()
        for raw_level_serie in raw_forecast.get("levelSeries"):
            self.pollen.append(Pollen(pollen_inst, raw_level_serie))

    def __repr__(self):
        return f"<Forecast for {self.region} with {len(self.pollen)} pollen records>"


class Pollen:
    name = None
    pollen_id = None
    level = -1
    time = None

    def __init__(self, pollen_inst, raw_level_serie):
        self.pollen_id = raw_level_serie.get("pollenId")
        self.name = pollen_inst.pollen_types.get(self.pollen_id)
        self.level = raw_level_serie.get("level")
        self.time = datetime.datetime.fromisoformat(raw_level_serie.get("time"))

    def __repr__(self):
        return f"<Pollen {self.name} level {self.level} at {self.time}>"


class PollenRapporten:
    base_url = "https://api.pollenrapporten.se/v1"
    regions = None
    pollen_types = None

    def __init__(self):
        self.get_regions()
        self.get_pollen_types()

    def api_call(self, path, params=None):
        resp = requests.get(self.base_url + path, params=params)
        resp.raise_for_status()
        return resp.json()

    def get_regions(self):
        resp = self.api_call("/regions")
        self.regions = dict()
        for region in resp.get("items"):
            self.regions[region.get("name", "")] = region.get("id")
        return self.regions

    def get_pollen_types(self):
        resp = self.api_call("/pollen-types")
        self.pollen_types = dict()
        for pollen in resp.get("items"):
            self.pollen_types[pollen.get("id", "")] = pollen.get("name")
        return self.pollen_types

    def get_forcast(self, region, current=True):
        # don't support these yet, start_date=None, end_date=None, offset=None)
        region_id = self.regions.get(region)
        assert region_id, f"No region by the name {region} found"
        params = {"region_id": region_id, "current": current}
        resp = self.api_call("/forecasts", params)
        return Forecast(self, resp.get("items")[0], region)


# MQTT / HA
def ha_discovery(config, pollen):
    """Announce to HA that we exists and our config for auto discovery"""
    if pollen:
        pollen_types = pollen.pollen_types.values()
    else:
        pollen_types = [  # just here to not spam them to much while testing
            "Gråbo",
            "Björk",
            "Al",
            "Alm",
            "Ek",
            "Malörtsambrosia",
            "Gräs",
            "Hassel",
            "Bok",
            "Sälg & Viden",
        ]

    formated_messages = []
    device_name = config.get("device_name")
    region_ha = ha_compat(config.get("region"))
    for pollen_type in pollen_types:
        pollen_ha = ha_compat(pollen_type)
        msg = {
            "state_topic": config["mqtt"].get("state_topic"),
            "value_template": f"{{{{ value_json.{pollen_ha} | int(0) }}}}",
            "unique_id": f"{device_name}-{region_ha}-{pollen_ha}",
            "state_class": "measurement",
            "icon": "mdi:flower-pollen",
            "name": pollen_type,
            "device": {
                "identifiers": [
                    f"{device_name}-{region_ha}",
                ],
                "name": f"Pollen in { config.get('region') }",
                "manufacturer": "blambi",
                "model": "PollerPoller",
                "sw_version": "git",
            },
        }
        formated_messages.append(
            {
                "topic": config["mqtt"]
                .get("config_topic")
                .replace("PLACEHOLDER", f"{device_name}-{pollen_ha}"),
                "payload": json.dumps(msg),
            }
        )

    mqtt_pub.multiple(
        formated_messages,
        hostname=config["mqtt"].get("host"),
        port=config["mqtt"].get("port"),
        auth=config["mqtt"].get("auth"),
    )
    logging.info("Announced to HA for auto discovery")


def ha_compat(word):
    """Internal like IDs etc for HA auto discovery requires it to fit in `[a-zA-Z0-9_-]+`"""
    return (
        word.lower()
        .replace("å", "ao")
        .replace("ä", "ae")
        .replace("ö", "oe")
        .replace(" ", "_")
    )


def publish_today(config, forecast):
    """Pushes the todays forecast to MQTT"""
    today = datetime.date.today()
    payload = {}
    for pollen in forecast.pollen:
        if pollen.time.date() == today:
            payload[ha_compat(pollen.name)] = pollen.level
    mqtt_pub.single(
        config["mqtt"].get("state_topic"),
        json.dumps(payload),
        hostname=config["mqtt"].get("host"),
        port=config["mqtt"].get("port"),
        auth=config["mqtt"].get("auth"),
    )


# internals
def get_config():
    def get_env(name, default=""):
        val = os.environ.get(name, default)
        if not default:
            assert val, f"Missing Required environ varible '{name}'"
        return val

    def get_env_int(name, default=-1):
        return int(get_env(name, default))

    conf = {
        "region": get_env("REGION"),
        "mqtt": {
            "host": get_env("MQTT_HOST"),
            "port": get_env_int("MQTT_PORT", 1883),
            "auth": {
                "username": get_env("MQTT_USER"),
                "password": get_env("MQTT_PASS"),
            },
        },
        "ha_base": get_env("HA_BASE", "homeassistant"),
        "device_name": get_env("DEV_NAME", "pollenpoller"),
    }

    # derived config
    conf["mqtt"].update(
        {
            "config_topic": f"{conf['ha_base']}/sensor/PLACEHOLDER/config",
            "state_topic": f"{conf['ha_base']}/sensor/{conf['device_name']}/state",
        }
    )
    return conf


def poll_and_publish(config, pollen):
    forecast = pollen.get_forcast(config.get("region"))
    publish_today(config, forecast)
    logging.info("Polled and published todays forecast")


if __name__ == "__main__" or True:
    logging.basicConfig(
        level=logging.INFO,
        stream=sys.stdout,
        format="%(asctime)s %(levelname)s: %(message)s",
    )
    logging.info(f"PollenPoller warming up")
    config = get_config()
    pollen = PollenRapporten()

    if not config.get("region") in pollen.regions:
        logging.error(
            f"Error: region '{config.get('region')}' not recognised please use one of: {', '.join(pollen.regions)}"
        )
        exit(1)
    ha_discovery(config, pollen)  # TODO: Check it worked?
    # ha_discovery(config, None) # For testing

    poll_hours = [1, 7, 13, 19]  # Only poll every 6hours and be kind of stupid about it
    logging.info(f"Polling hours: {poll_hours}")
    last = datetime.datetime.now()
    poll_and_publish(config, pollen)

    while True:
        now = datetime.datetime.now()
        if now.hour in poll_hours and (now - last) >= datetime.timedelta(hours=1):
            poll_and_publish(config, pollen)
        time.sleep(3600)  # one hour
